import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { BeerContainer } from './beerContainer.model';
import { Beer } from './beer.model';

@Injectable({
  providedIn: 'root'
})

export class BreweriService {
  
  // Define API
  apiURL = 'http://localhost:8001/breweri';

  constructor(private http: HttpClient) { }

  /*========================================
    CRUD Methods for consuming RESTful API
  =========================================*/

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json'
    })
  }  

  
  getAllContainers(): Observable<BeerContainer[]> {
    return this.http.get<BeerContainer[]>(this.apiURL + '/all')
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  createContainer(): Observable<BeerContainer> {
      return this.http.post<BeerContainer>(this.apiURL + '/create', null)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  openContainerDoor(containerId:number): Observable<BeerContainer> {
    return this.http.put<BeerContainer>(this.apiURL + '/'+containerId+'/openDoor', null)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  closeContainerDoor(containerId:number): Observable<BeerContainer> {
    return this.http.put<BeerContainer>(this.apiURL + '/'+containerId+'/closeDoor', null)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  addBeerToContainer(containerId:number, beer: Beer): Observable<Beer> {
    return this.http.post<Beer>(this.apiURL + '/'+containerId+'/create', beer)
    .pipe(
        retry(1),
        catchError(this.handleError)
    )
  }

  getBeerFromContainer(containerId: number, beerId: number): Observable<Beer> {
    return this.http.delete<Beer>(this.apiURL + '/'+containerId+'/'+beerId)
    .pipe(
        retry(1),
        catchError(this.handleError)
    ) 
  }

/*
  // HttpClient API get() method => Fetch employee
  getEmployee(id): Observable<Employee> {
    return this.http.get<Employee>(this.apiURL + '/employees/' + id)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }  

  // HttpClient API post() method => Create employee
  createEmployee(employee): Observable<Employee> {
    return this.http.post<Employee>(this.apiURL + '/employees', JSON.stringify(employee), this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }  
*/

  // Error handling 
  handleError(error) {
     let errorMessage = '';
     if(error.error instanceof ErrorEvent) {
       // Get client-side error
       errorMessage = error.error.message;
     } else {
       // Get server-side error
       errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
     }
     window.alert(errorMessage);
     return throwError(errorMessage);
  }

}