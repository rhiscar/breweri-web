import { Beer } from './beer.model';

export class BeerContainer {
    public beerList: Beer[];
    public temp: number;
    public containerId: number;
    public doorOpened: boolean;

    constructor(containerId: number) {
        this.temp = -10;
        this.beerList = [];
        this.containerId = containerId;
    }
}